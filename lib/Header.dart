import 'package:flutter/material.dart';
import 'BackPolitica.dart';

class Header extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Widget textSection = Container(
      padding: const EdgeInsets.all(28),
      child: Text(
                'Mediante el presente AVISO DE PRIVACIDAD, TULKIT 3.0, S.A.P.I. DE C.V. Informan a Usted los términos '
                    'en que serán tratados los Datos Personales que recaben, por lo cual, le recomendamos que lea atentamente la '
                'siguiente información:',
        style: const TextStyle(
            fontSize: 15.0,
            color: Color(0xFF424242)
        ),
        softWrap: true,
      ),
    );

    return new Scaffold(
      body: new Stack(
        children: <Widget>[
          new BackPolitica(),
          new Container(
            alignment: Alignment.center,
            margin: new EdgeInsets.only(
              top: 50.0
            ),
              child: new Column(
              children: <Widget>[
                new Text(
              "Política de Privacidad",
            style: const TextStyle(
              fontSize: 25.0,
              color: Color(0xFF424242),
              fontWeight: FontWeight.w600
            ),
          ),
                textSection
              ],

          ),
          )
        ],
      ),
    );
  }

}